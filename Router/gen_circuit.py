import sys
import argparse
import random

class Net:
    def __init__(self):
        self.src_pin = (-1, -1, -1)
        self.dest_pins = []
        return

class Bbox:
    def __init__(self, xlow, ylow, xhigh, yhigh):
        self.xlow = xlow
        self.ylow = ylow
        self.xhigh = xhigh
        self.yhigh = yhigh
        return

    def xrange(self):
        return xrange(self.xlow, self.xhigh + 1)

    def yrange(self):
        return xrange(self.ylow, self.yhigh + 1)

def elaborate_and_shuffle_bbox(bbox):
    pins = []
    for ix in bbox.xrange():
        for iy in bbox.yrange():
            for ipin in xrange(1, 4 + 1):
                pins.append((ix, iy, ipin))
    random.shuffle(pins)
    return pins


def generate_nets_in_bbox(nets, used_ipins, used_opins, bbox, max_nets):
    max_fanouts = 1

    shuffled_pins = elaborate_and_shuffle_bbox(bbox)
    
    for src_pin in shuffled_pins:
        if src_pin in used_ipins or src_pin in used_opins:
            continue

        src_net = Net()
        src_net.src_pin = src_pin
        num_fanouts = 0

        for dest_pin in shuffled_pins:
            if num_fanouts >= max_fanouts:
                break

            if dest_pin in used_ipins or dest_pin in used_opins:
                continue
            if src_pin == dest_pin:
                continue

            if src_pin not in used_opins:
                used_opins.add(src_pin)
            src_net.dest_pins.append(dest_pin)
            num_fanouts += 1
            used_ipins.add(dest_pin)
        if len(src_net.dest_pins) > 0:
            nets.append(src_net)
        if len(nets) >= max_nets:
            return
    return

def dump_circuit(nets, args):
    with open(args.out, 'w') as fp:
        fp.write('%d\n'%args.dim)
        fp.write('%d\n'%args.channel)
        for inet in nets:
            for idest_pin in inet.dest_pins:
                fp.write('%d %d %d %d %d %d\n'%(
                    inet.src_pin[0],
                    inet.src_pin[1],
                    inet.src_pin[2],
                    idest_pin[0],
                    idest_pin[1],
                    idest_pin[2]
                    )
                )
        fp.write('-1 -1 -1 -1 -1 -1\n')
    return

if __name__ == '__main__':
    random.seed(0)
    parser = argparse.ArgumentParser()
    parser.add_argument('--dim', type=int, default=100, help='Rows and Columns in device')
    parser.add_argument('--nets', type=int, default=1, help='Number of nets to generate')
    parser.add_argument('--channel', type=int, default=36, help='Channel width of device')
    parser.add_argument('--out', required=True, help='Output circuit filename')
    args = parser.parse_args()

    print 'Info: Generating circuit for Device with dimension %d'%args.dim
    print 'Info: Channel width %d'%args.channel
    print 'Info: %d nets'%args.nets
    print 'Info: Output file %s'%args.out

    # Set of tuples (blockx, blocky, pin_idx)
    used_ipins = set()
    used_opins = set()

    nets = []
    bbox_dims = (0.05, 0.1, 0.2, 0.5)
    last_len = 0
    while len(nets) < args.nets:
        for ibbox_xdim in bbox_dims:
            if len(nets) >= args.nets:
                break
            xwidth = int(args.dim * ibbox_xdim)
            if xwidth < 1:
                continue
            for ibbox_ydim in bbox_dims:
                if len(nets) >= args.nets:
                    break
                ywidth = int(args.dim * ibbox_ydim)

                # Choose a random center point
                cx, cy = random.randint(0, args.dim - 1), random.randint(0, args.dim - 1)

                xlow, ylow = int(cx - xwidth * 0.5), int(cy - ywidth * 0.5)
                xhigh, yhigh = int(cx + xwidth * 0.5), int(cy + ywidth * 0.5)

                # Shift bbox to fix in device if necessary
                if xlow < 0:
                    xhigh -= xlow
                    xlow = 0
                    assert xhigh < args.dim
                if xhigh >= args.dim:
                    dx = xhigh - args.dim + 1
                    xlow -= dx
                    xhigh = args.dim - 1
                    assert xlow >= 0
                if ylow < 0:
                    yhigh -= ylow
                    ylow = 0
                    assert yhigh < args.dim
                if yhigh >= args.dim:
                    dy = yhigh - args.dim + 1
                    ylow -= dy
                    yhigh = args.dim - 1
                    assert ylow >= 0

                assert xlow >= 0
                assert ylow >= 0
                assert xhigh < args.dim
                assert yhigh < args.dim
                assert xlow < xhigh
                assert ylow < yhigh

                generate_nets_in_bbox(nets, used_ipins, used_opins,
                    Bbox(xlow, ylow, xhigh, yhigh), args.nets)




        if len(nets) == last_len:
            print 'Error: Unable to create %d nets. %d nets created so far'%(args.nets, len(nets))
            exit(-1)
        last_len = len(nets)
    dump_circuit(nets, args)


