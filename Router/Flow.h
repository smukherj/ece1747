#ifndef FLOW_H
#define FLOW_H

void process_input_params(const char *circuit_filename);
bool run_router_flow();
void draw_routed_circuit();
#endif