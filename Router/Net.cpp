#include "Net.h"
#include "Node.h"
#include "Graph.h"
#include "Utils.h"

namespace router
{
	bool Net::routed() const
	{
		rt_assert(route.empty() || temp_route.empty());
		bool result = !route.empty() || !temp_route.empty();
		return result;
	}

	void Net::commit()
	{
		rt_assert(routed());
		rt_assert(route.empty());
		rt_assert(!temp_route.empty());
		route = temp_route;
		temp_route.clear();
	}

	void Net::dump_route(FILE *fh, const Graph& g)
	{
		rt_assert(routed());
		const Node& src_node = g.node(src);
		const Node& dest_node = g.node(dest);
		fprintf(fh, "Block (%d, %d) pin %d -> Block (%d, %d) pin %d (cost %llu)\n",
			src_node.x(),
			src_node.y(),
			src_node.z(),
			dest_node.x(),
			dest_node.y(),
			dest_node.z(),
			route_cost
		);
		fprintf(fh, "{\n");
		for(size_t i = 0; i < route.size(); ++i)
		{
			Node::ID id = route[i];
			const Node& node = g.node(id);
			fprintf(fh, "   %s (%d, %d, %d) \n", 
				Node::type_str(node.type()),
				node.x(),
				node.y(),
				node.z()
			);
		}
		fprintf(fh, "}\n");
	}
}