#ifndef NET_H
#define NET_H

#include <stdio.h>
#include <vector>
#include "Node.h"

namespace router
{
	class Graph;
	struct Net
	{
		Node::ID src;
		Node::ID dest;
		// Final committed route
		std::vector<Node::ID> route;
		// Route stored while routing to be
		// committed later
		std::vector<Node::ID> temp_route;
		unsigned long long route_cost;
		bool congested;

		Net() :
		src(Node::ILLEGAL_ID),
		dest(Node::ILLEGAL_ID),
		route_cost(0),
		congested(false)
		{ }

		bool routed() const;

		// Move route from "temp_route" to "route"
		void commit();

		void rip_up() { route.clear(); temp_route.clear(); }

		void dump_route(FILE* fh, const Graph& g);
	};

	typedef std::vector<Net> Netlist;
}

#endif /* NET_H */