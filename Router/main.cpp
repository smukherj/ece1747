#include "Flow.h"
#include "Utils.h"

int main(int argc, char *argv[])
{
	int result = 0;
	msg::info("Compiled on %s, %s", __DATE__, __TIME__);
#ifdef DEBUG
	msg::info("This is a DEBUG build.");
#else
	msg::info("This is an OPTIMIZED build.");
#endif
	if(argc < 2)
	{
		msg::error("Expected atleast one commandline argument pointing to circuit description file");
		return 1;
	}
	utils::parse_cmd_args(argc - 2, argv + 2);
	process_input_params(argv[1]);
	if(run_router_flow())
	{
		msg::info("Routing was successful.");
	}
	else
	{
		msg::error("Routing FAILED!");
		result = 1;
	}
	draw_routed_circuit();
	return result;
}