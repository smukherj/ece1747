#include "ParallelHeap.h"

#include <cassert>
#include <cstdlib>

#include <chrono>
#include <iostream>

#include <boost/heap/binomial_heap.hpp>

void test()
{
  ParallelHeap<int, 16> heap;
  assert(heap.empty());
  for (int i = 64; i > 0; i--) {
    heap.push(i);
  }

  heap.pop();
  assert(heap.top() == 2);
  heap.pop();
  assert(heap.top() == 3);
  heap.clear();
  assert(heap.empty());

  heap.push(3);
  assert(heap.size() == 1U);
  assert(heap.top() == 3);

  for (int i = 0; i < 64; i++) {
    heap.push(i);
  }

  while (!heap.empty()) {
    heap.pop();
  }
}

template <class T>
void benchmark()
{
  auto begin_wc = std::chrono::system_clock::now();

  T heap;
  srand(42);
  for (int i = 0; i < 50000; i++) {
    heap.push(rand());
  }

  assert(heap.top() == 3889);

  auto end_wc = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end_wc - begin_wc;
  double wc_time = elapsed_seconds.count();

  std::cout << "WC time: " << wc_time << '\n';
}

int main()
{
  test();

  std::cout << "Parallel Heap:\n";
  benchmark<ParallelHeap<int, 16>>();
  std::cout << "Priority Queue:\n";
  benchmark<std::priority_queue<int, std::vector<int>, std::greater<int>>>();
  std::cout << "Binomial Heap:\n";
  benchmark<boost::heap::binomial_heap<int, boost::heap::compare<std::greater<int>>>>();

  return 0;
}
