#ifndef ROUTE_ALGORITHM_H
#define ROUTE_ALGORITHM_H

#include "Net.h"

namespace router
{
	class Graph;

	bool run_route_algorithm(const Graph& g, Netlist& netlist);
}

#endif /* ROUTE_ALGORITHM_H */