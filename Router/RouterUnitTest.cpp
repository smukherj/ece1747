#include "parallel_task_queue.h"
#define BOOST_TEST_MODULE RouterUnitTest
#include <boost/test/unit_test.hpp>
#include <mutex>

// tq is short for 'task_queue', not Test Quartus
BOOST_AUTO_TEST_CASE( tq_create_and_destroy )
{
    struct ThreadData
    {
    	int data;
    };

    parallel::task_scheduler<ThreadData> t;
    t.addTask(ThreadData());
}

BOOST_AUTO_TEST_CASE( tq_incr_counter )
{

    std::mutex m;
    int counter = 0;

    struct ThreadData
    {
    	int data = 0;
    };

    auto thread_callback = [&m, &counter] (ThreadData& t) -> void
    {
    	std::lock_guard<std::mutex> lock(m);
    	counter += t.data;
    };

    size_t num_calls = 10000;

    {
    	parallel::task_scheduler<ThreadData> t;
    	t.attachCallback(thread_callback);

    	for(auto i = 0; 
    		i < num_calls; ++i)
    	{
    		ThreadData td;
    		td.data = 1;
    		t.addTask(td);
    	}
    }

    BOOST_CHECK_EQUAL(counter, num_calls);
}

// ttq is short for targeted_task_queue
BOOST_AUTO_TEST_CASE( ttq_create_one_and_destroy )
{
    struct ThreadData
    {
    	int data;
    };

    parallel::targeted_task_scheduler<ThreadData> t(1);
    t.dispatch();
    t.addTask(0, ThreadData());
}

BOOST_AUTO_TEST_CASE( ttq_create_no_dispatch )
{
    struct ThreadData
    {
    	int data;
    };

    parallel::targeted_task_scheduler<ThreadData> t;
    parallel::targeted_task_scheduler<ThreadData> t1(1);
}

BOOST_AUTO_TEST_CASE( ttq_create_default_and_destroy )
{
    struct ThreadData
    {
    	int data;
    };

    parallel::targeted_task_scheduler<ThreadData> t;
    t.dispatch();

    for(size_t i = 0; i < t.num_threads(); ++i)
    {
    	t.addTask(i, ThreadData());
    }
}

BOOST_AUTO_TEST_CASE( ttq_incr_counter )
{

    std::mutex m;
    int counter = 0;

    struct ThreadData
    {
    	int data = 0;
    };

    auto thread_callback = [&m, &counter] (ThreadData& t) -> void
    {
    	std::lock_guard<std::mutex> lock(m);
    	counter += t.data;
    };

    size_t num_calls = 10000;

    {
    	parallel::targeted_task_scheduler<ThreadData> t;
    	size_t tid = 0;
    	t.attachCallback(thread_callback);
    	t.dispatch();

    	for(size_t i = 0; 
    		i < num_calls; ++i)
    	{
    		ThreadData td;
    		td.data = 1;
    		t.addTask(tid, td);

    		tid = (tid + 1) % t.num_threads();
    	}
    }

    BOOST_CHECK_EQUAL(counter, num_calls);
}