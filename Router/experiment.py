import subprocess
import sys
import re
import time

WIRE_USAGE_RE = re.compile('Routed circuit uses (\d+) wires')

class Result:
	def __init__(self):
		self.success = False
		self.wire_usage = -1
		self.channel_width = -1
		self.circuit = -1
		self.swbox_type = None

	def log(self, fp=sys.stdout):
		if self.success:
			fp.write('%d, %d, %d, %s\n'%(self.circuit, self.wire_usage, self.channel_width, self.swbox_type))
			fp.flush()
		return

def run_testcase(circuit, swbox_type, channel_width):
	result = Result()
	p = subprocess.Popen(['./Router', 'cct%d.txt'%circuit, '-no_graphics', '-override_channel_width=%s'%channel_width, '-swbox_type=%s'%swbox_type],
		stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	result.success = (p.wait() == 0)
	for line in p.stdout:
		match = WIRE_USAGE_RE.search(line)
		if match:
			result.wire_usage = int(match.group(1))
	result.channel_width = channel_width
	result.swbox_type = swbox_type
	result.circuit = circuit
	return result

fp = open('experiment_results.txt', 'w')
start_time = time.time()
for icircuit in range(1, 4 + 1):
	print 'Running experiment for circuit', icircuit
	for swbox_type in ('unidir', 'bidir'):
		print '   Testing switchbox type', swbox_type
		last_result = None
		for channel_width in range(12, 0, -2):
			print '      Running testcase with channel_width', channel_width
			result = run_testcase(icircuit, swbox_type, channel_width)
			if not result.success:
				print '      Routing FAILED at channel_width', channel_width
				break
			else:
				last_result = result
		if last_result:
			last_result.log(fp)
		else:
			print 'Error: No results for circuit %d switchbox type %s'%(icircuit, swbox_type)
print 'Experiment runtime was %.0fs'%(time.time() - start_time)