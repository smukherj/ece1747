#include "Node.h"
#include "Graph.h"
#include "Net.h"
#include "Utils.h"
#include "RouteAlgorithm.h"
#include <algorithm>
#include <list>
#include <queue>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <chrono>
#include <tbb/parallel_for.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/enumerable_thread_specific.h>

#include "parallel_task_queue.h"

namespace
{
	using namespace router;

	typedef unsigned long long HeapCost;
	const HeapCost MAX_HEAP_COST = static_cast<unsigned int>(-1);
	const int NEGOTIATED_CONGESTION_TRANSITION = 0;
	HeapCost CONGESTION_BASE_PENALTY = 0;
	HeapCost CONGESTION_AVOIDANCE_PENALTY = 1;


	struct RRNode
	{
		typedef std::map<Node::ID, int> RoutedPinUsageMap;

		Node::ID id;
		int usage;
		HeapCost base_cost;
		int num_congested_iterations;
		// Map of source pins that were routed
		// through this node to the number of times
		// a routed from this source pin was routed
		// via this node
		RoutedPinUsageMap routed_pins;

		RRNode() :
		id(Node::ILLEGAL_ID),
		usage(0),
		base_cost(0),
		num_congested_iterations(0)
		{ }
	};

	struct HeapNode
	{
		Node::ID id;
		HeapCost cost;

		HeapNode() :
		id(Node::ILLEGAL_ID),
		cost(MAX_HEAP_COST)
		{ }
	};

	struct HeapNodeCompare
	{
		bool operator () (const HeapNode &lhs, const HeapNode &rhs) const
		{
			return lhs.cost > rhs.cost;
		}
	};

	typedef std::priority_queue<HeapNode, std::vector<HeapNode>, HeapNodeCompare> RouterHeap;

	typedef std::map<Node::ID, std::vector<size_t> > PinToNetMap;

	struct ThreadLocalData
	{
		RouterHeap expansion_heap;
		std::unordered_map<Node::ID, Node::ID> prev_map;
	};

	typedef tbb::enumerable_thread_specific<ThreadLocalData> RouteWorkerData;


	
	// Data not modified while routing nets
	RouteWorkerData wd_;
	std::vector<RRNode> rr_nodes_; // Does have data modifed during routing. Must be fixed
	PinToNetMap pin_to_nets_;
	const Graph *graph_ = NULL;
	const Netlist *netlist_ = NULL;
	int router_iteration_ = -1;

	void set_prev(Node::ID id, Node::ID prev)
	{
		auto& wd = wd_.local();
		wd.prev_map[id] = prev;
	}

	Node::ID get_prev(Node::ID id)
	{
		auto& wd = wd_.local();
		auto it = wd.prev_map.find(id);

		if(it == wd.prev_map.end())
		{
			return Node::ILLEGAL_ID;
		}
		else
		{
			return it->second;
		}
	}

	void build_rr_nodes()
	{
		rr_nodes_.resize(graph_->num_nodes() + 1);
		for(Graph::node_const_iterator it = graph_->begin_nodes(); it != graph_->end_nodes(); ++it)
		{
			const Node& node = *it;
			RRNode rr_node;
			rr_node.id = node.id();
			rr_nodes_.at(rr_node.id) = rr_node;
		}
	}

	void build_pin_to_net_map()
	{
		pin_to_nets_.clear();
		for(size_t inet = 0; inet < netlist_->size(); ++inet)
		{
			const Net& net = netlist_->at(inet);
			pin_to_nets_[net.src].push_back(inet);
		}
	}

	HeapCost get_congestion_cost(Node::ID source_pin, const RRNode& rr_node)
	{
		HeapCost congestion_penalty = 0;
		if(rr_node.usage > 0 && rr_node.routed_pins.find(source_pin) == rr_node.routed_pins.end())
		{
			congestion_penalty = CONGESTION_AVOIDANCE_PENALTY;
		}
		return congestion_penalty;
	}

	void initialize_heap(Node::ID source_pin, Node::ID dest_pin)
	{
		auto& local_wd = wd_.local();
		
		local_wd.prev_map.clear();
		local_wd.expansion_heap = RouterHeap();
		set_prev(source_pin, Node::ILLEGAL_ID);

		std::unordered_map<int, int> id_to_cost;

		Graph::node_id_const_iterator it = graph_->begin_fanouts(source_pin);
		Graph::node_id_const_iterator end = graph_->end_fanouts(source_pin);
		for(; it != end; ++it)
		{
			Node::ID track_id = *it;
			const Node& node = graph_->node(track_id);
			rt_assert(node.is_wire());
			RRNode& rr_node = rr_nodes_.at(track_id);

			HeapNode heap_node;
			heap_node.id = track_id;
			heap_node.cost = get_congestion_cost(source_pin, rr_node);
			id_to_cost.emplace(track_id, heap_node.cost);
			local_wd.expansion_heap.push(heap_node);
			set_prev(track_id, source_pin);
		}
		for(size_t inet : pin_to_nets_[source_pin])
		{
			const Net& net = netlist_->at(inet);
			if(!net.routed())
			{
				continue;
			}
			if(net.src == source_pin && net.dest == dest_pin)
			{
				continue;
			}
			Node::ID prev_id = Node::ILLEGAL_ID;
			for(Node::ID id : net.route)
			{
				RRNode& rr_node = rr_nodes_.at(id);

				if(id_to_cost.count(id) == 0U)
				{
					HeapNode heap_node;
					heap_node.id = id;
					HeapCost prev_cost = 0;
					if(prev_id != Node::ILLEGAL_ID)
					{
						prev_cost = 1 + id_to_cost.at(prev_id);
					}
					heap_node.cost = get_congestion_cost(source_pin, rr_node) + prev_cost;
					local_wd.expansion_heap.push(heap_node);
					set_prev(id, prev_id);
					id_to_cost.emplace(id, heap_node.cost);
				}
				prev_id = id;
			}
		}
	}

	std::unordered_set<Node::ID> get_target_tracks(Node::ID destination_pin)
	{
		std::unordered_set<Node::ID> result;
		Graph::node_id_const_iterator it = graph_->begin_fanins(destination_pin);
		Graph::node_id_const_iterator end = graph_->end_fanins(destination_pin);
		for(; it != end; ++it)
		{
			Node::ID track_id = *it;
			const Node& node = graph_->node(track_id);
			rt_assert(node.type() == Node::VTRACK || node.type() == Node::HTRACK);
			RRNode& rr_node = rr_nodes_.at(track_id);
			rt_assert(rr_node.id == track_id);
			result.insert(track_id);
			//msg::debug("Adding target track %llu", track_id);
		}
		return result;
	}

	HeapCost get_node_cost(Node::ID source_pin, const Node& node, const Node& target_node)
	{
		HeapCost cost = 0;
		HeapCost congestion_penalty = 0;
		const RRNode& rr_node = rr_nodes_.at(node.id());
		congestion_penalty = get_congestion_cost(source_pin, rr_node);
		cost = 1 + congestion_penalty + rr_node.base_cost;
		return cost;
	}

	void add_neighbours_to_heap(Node::ID source_pin, const HeapNode& heap_node, const Node& target_node, const std::unordered_set<Node::ID>& visited)
	{
		auto& local_wd = wd_.local();
		Graph::node_id_const_iterator it = graph_->begin_fanouts(heap_node.id);
		Graph::node_id_const_iterator end = graph_->end_fanouts(heap_node.id);
		for(; it != end; ++it)
		{
			Node::ID fanout_id = *it;
			if(visited.count(fanout_id) == 1U)
			{
				continue;
			}
			const Node& fanout_node = graph_->node(fanout_id);
			if(fanout_node.type() == Node::TERMINAL)
			{
				continue;
			}

			HeapNode fanout_heap_node;
			fanout_heap_node.id = fanout_id;
			fanout_heap_node.cost = heap_node.cost + get_node_cost(source_pin, fanout_node, target_node);
			local_wd.expansion_heap.push(fanout_heap_node);
			set_prev(fanout_id, heap_node.id);
		}
	}

	bool use_rr_node(Node::ID node_id, Node::ID src)
	{
		RRNode& rr_node = rr_nodes_.at(node_id);
		if(rr_node.routed_pins.find(src) == rr_node.routed_pins.end())
		{
			rr_node.routed_pins[src] = 0;
			rr_node.usage++;
		}
		rr_node.routed_pins[src]++;

		return rr_node.usage < 2;
	}

	bool commit_route(Net& n)
	{
		bool result = true;
		n.commit();
		rt_assert(n.routed());

		for(Node::ID id : n.route)
		{
			result = use_rr_node(id, n.src) && result;
		}
		n.congested = !result;
		return result;
	}

	void save_route(Net& n)
	{
		const RRNode& target_pin_rr_node = rr_nodes_.at(n.dest);
		// If this assertion fails, it means the graph was disconnected
		// and we should have never reached here. The flow should have
		// errored out earlier.
		rt_assert(get_prev(n.dest) != Node::ILLEGAL_ID);
		std::list<Node::ID> route;

		Node::ID cur_id = target_pin_rr_node.id;
		do
		{
			//msg::debug("Trace %llu", cur_id);
			route.push_front(cur_id);
			cur_id = get_prev(cur_id);
		}
		while(cur_id != Node::ILLEGAL_ID);

		rt_assert(route.front() == n.src);
		rt_assert(route.back() == n.dest);
		rt_assert(!n.routed());
		n.temp_route.insert(n.temp_route.begin(), route.begin(), route.end());
	}

	bool route_net(Net& n)
	{
		auto& local_wd = wd_.local();
		rt_assert(n.src != n.dest);
		bool result = false;
		const Node& source_node = graph_->node(n.src);
		const Node& target_node = graph_->node(n.dest);
#if 0
		if(router_iteration_ > 0)
		{
			msg::debug("Routing %llu (%d, %d, %d) to %llu (%d, %d, %d). Cost %llu", 
				n.src,
				source_node.x(),
				source_node.y(),
				source_node.z(),
				n.dest, 
				target_node.x(),
				target_node.y(),
				target_node.z(),
				n.route_cost);
		}
#endif
		initialize_heap(n.src, n.dest);
		std::unordered_set<Node::ID> visited;
		visited.reserve(1000);
		std::unordered_set<Node::ID> target_tracks = get_target_tracks(n.dest);
		rt_assert(source_node.type() == Node::TERMINAL);
		rt_assert(target_node.type() == Node::TERMINAL);
		while(!local_wd.expansion_heap.empty())
		{
			const HeapNode const_heap_node = local_wd.expansion_heap.top();
			local_wd.expansion_heap.pop();
			rt_assert(const_heap_node.cost != MAX_HEAP_COST);
			bool inserted = visited.insert(const_heap_node.id).second;
			if (inserted)
			{
				if(target_tracks.find(const_heap_node.id) != target_tracks.end())
				{
					RRNode& target_pin_rr_node = rr_nodes_.at(n.dest);
					set_prev(target_pin_rr_node.id, const_heap_node.id);

					n.route_cost = const_heap_node.cost;
					// We have reached a target track.
					// TODO: Continue searching if current path is congested
					result = true;
					break;
				}
				add_neighbours_to_heap(n.src, const_heap_node, target_node, visited);
			}

		}
		//msg::debug("Done routing %llu to %llu", n.src, n.dest);
		if(!result)
		{
			msg::error("Disconnected routing graph detected!");
			msg::error("Failed to route from source pin %llu to destination_pin %llu.",
				n.src,
				n.dest);
			rt_die;
		}
		save_route(n);

		return result;
	}


	void unuse_rr_node(Node::ID node_id, Node::ID src)
	{
		const Node& node = graph_->node(node_id);
		if(node.type() == Node::TERMINAL)
		{
			return;
		}

		RRNode& rr_node = rr_nodes_.at(node_id);
		RRNode::RoutedPinUsageMap::iterator it = rr_node.routed_pins.find(src);
		rt_assert(it != rr_node.routed_pins.end());
		rt_assert(it->second > 0);
		rt_assert(rr_node.usage > 0);
		it->second--;
		if(it->second == 0)
		{
			rr_node.routed_pins.erase(it);
			rr_node.usage--;
		}

	}

	void rip_up_net(Net& net)
	{
		if(!net.routed())
		{
			return;
		}
		for(size_t i = 0; i < net.route.size(); ++i)
		{
			unuse_rr_node(net.route.at(i), net.src);
		}
		net.rip_up();
	}

	void report_congestion_stats(const Netlist& netlist, const std::unordered_set<int>& congested_nets)
	{
		std::unordered_set<int>::const_iterator it;
		msg::info("   Congested nets:-");
		for(it = congested_nets.begin(); it != congested_nets.end(); ++it)
		{
			int inet = *it;
			const Net& n = netlist.at(inet);
			const Node& source_pin = graph_->node(n.src);
			const Node& dest_pin = graph_->node(n.dest);
			msg::info("      (%d, %d) pin %d -> (%d, %d) pin %d", source_pin.x(),
				source_pin.y(),
				source_pin.z(),
				dest_pin.x(),
				dest_pin.y(),
				dest_pin.z()
			);
			msg::info("      Overused nodes:-");
			for(size_t inode = 0; inode < netlist.at(inet).route.size(); ++inode)
			{
				const RRNode& rr_node = rr_nodes_.at(netlist.at(inet).route.at(inode));
				if(rr_node.usage > 1)
				{
					const Node& node = graph_->node(rr_node.id);
					msg::info("         %s (%d, %d, %d)",
						Node::type_str(node.type()),
						node.x(),
						node.y(),
						node.z());
					msg::info("         Source pins:-");
					RRNode::RoutedPinUsageMap::const_iterator it;
					for(it = rr_node.routed_pins.begin(); it != rr_node.routed_pins.end(); ++it)
					{
						Node::ID routed_pin = it->first;
						const Node& routed_pin_node = graph_->node(routed_pin);
						msg::info("            (%d, %d) pin %d", routed_pin_node.x(),
							routed_pin_node.y(),
							routed_pin_node.z());
					}	
				}
			}
		}
	}

	void adjust_base_penalties()
	{
		if (graph_->num_nodes() > 350)
		{
			return;
		}
		for(size_t irr_node = 0; irr_node < rr_nodes_.size(); ++irr_node)
		{
			if(rr_nodes_[irr_node].usage > 1)
			{
				rr_nodes_[irr_node].base_cost += CONGESTION_BASE_PENALTY;
				rr_nodes_[irr_node].num_congested_iterations++;
			}
		}
	}

	void rip_up_all_nets(Netlist& netlist)
	{
		for(size_t inet = 0; inet < netlist.size(); ++inet)
		{
			netlist[inet].rip_up();
		}

		for(size_t irr_node = 0; irr_node < rr_nodes_.size(); ++irr_node)
		{
			if(rr_nodes_[irr_node].usage > 1)
			{
				rr_nodes_[irr_node].base_cost += CONGESTION_BASE_PENALTY;
				rr_nodes_[irr_node].num_congested_iterations++;
			}
			rr_nodes_[irr_node].usage = 0;
			rr_nodes_[irr_node].routed_pins.clear();
		}

	}

#if 0
	bool compare_net(const Net& lhs, const Net& rhs)
	{
		if(lhs.congested == rhs.congested)
		{
			return lhs.route_cost < rhs.route_cost;
		}
		else if(lhs.congested)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
#endif


	void cleanup()
	{
		wd_.clear();
		pin_to_nets_.clear();
		rr_nodes_.clear();
		graph_ = NULL;
		netlist_ = NULL;
		router_iteration_ = -1;
	}

	void check_routed_netlist(const Netlist& netlist)
	{
		std::unordered_set<Node::ID> unique_nodes;
		msg::info("Checking the routed netlist.");
		for(size_t inet = 0; inet < netlist.size(); ++inet)
		{
			const Net& net = netlist[inet];
			rt_assert(!net.route.empty());
			for(size_t irr_node = 0; irr_node < net.route.size(); ++irr_node)
			{
				const RRNode& rr_node = rr_nodes_.at(net.route[irr_node]);
				const Node& node = graph_->node(rr_node.id);
				if(node.is_wire())
				{
					unique_nodes.insert(node.id());
				}
				//msg::debug("inet %llu, node %llu usage %d", inet, rr_node.id, rr_node.usage);
				rt_assert(rr_node.usage == 1);
				rt_assert(rr_node.routed_pins.find(net.src) != rr_node.routed_pins.end());
				rt_assert(rr_node.routed_pins.size() == 1);
			}
		}
		msg::info("Netlist checks complete.");
		msg::info("   Routed circuit uses %zu wires.", unique_nodes.size());
	}

#if 0
	void report_nets(const Netlist& netlist)
	{
		for(const Net& net : netlist)
		{
			msg::debug("Net route size: %zu, cost: %llu, congested: %s",
				net.route.size(),
				net.route_cost,
				net.congested ? "yes" : "no");
		}
	}
#endif

	namespace partitioned
	{
		enum class ETask
		{
			DISPATCH_SIGNAL,
			EXPAND_NODE,
			COMMIT_NET
		};

		union TaskUnion
		{
			size_t inet;
			Node::ID inode;
		};

		struct TaskData
		{
			ETask task_code;
			TaskUnion tu;
		};

		using router_task_q_t = parallel::targeted_task_scheduler<TaskData>;

		// Variables global in the 'partitioned' namespace
		namespace global
		{
			std::shared_ptr<router_task_q_t> rtq;
		} /* namespace global */

		void signal_dispatcher(TaskData td)
		{
			// Do the initialization step for this signal
			// and add the source nodes to the task queue
		}

		void node_expansion_handler(TaskData td)
		{
			// Keep expanding this node until we run out of our
			// allocated boundary, exhaust the search or hit our
			// target
		}

		void netlist_committer(TaskData td)
		{
			// Commit this signal to the netlist
		}

		void dispatcher(TaskData td)
		{
			switch(td.task_code)
			{
				case ETask::EXPAND_NODE: node_expansion_handler(td); break;
				case ETask::DISPATCH_SIGNAL: signal_dispatcher(td); break;
				case ETask::COMMIT_NET: netlist_committer(td); break;
			}
		}
		
	} /* namespace partitioned */

	void collect_nodes_in_nets(const std::unordered_set<int>& nets,
		std::unordered_set<Node::ID>& nodes)
	{
		for(auto inet : nets)
		{
			const Net& net = netlist_->at(inet);
			for(auto inode : net.route)
			{
				nodes.insert(inode);
			}

		}
	}

	void collect_congested_nets(std::unordered_set<int>& congested_nets)
	{
		congested_nets.clear();
		size_t num_nets = netlist_->size();
		for(size_t inet = 0; inet < num_nets; ++inet)
		{
			const Net& net = netlist_->at(inet);
			for(auto inode : net.route)
			{
				//msg::debug("Net %zu inode %zu usage %d",
					//inet, inode, rr_nodes_[inode].usage);
				if(rr_nodes_[inode].usage > 1)
				{
					congested_nets.insert(inet);
					break;
				}
			}
		}
	}

	void collect_nets_with_nodes(std::unordered_set<int>& nets,
		const std::unordered_set<Node::ID>& nodes)
	{
		nets.clear();
		size_t num_nets = netlist_->size();
		for(size_t inet = 0; inet < num_nets; ++inet)
		{
			const Net& net = netlist_->at(inet);
			for(auto inode : net.route)
			{
				if(nodes.find(inode) != nodes.end())
				{
					nets.insert(inet);
					break;
				}
			}
		}
	}

	void expand_congested_nets(std::unordered_set<int>& congested_nets)
	{
		collect_congested_nets(congested_nets);
		if(router_iteration_ > 100)
		{
			std::unordered_set<Node::ID> nodes;
			collect_nodes_in_nets(congested_nets, nodes);
			collect_nets_with_nodes(congested_nets, nodes);
			for(auto inet : congested_nets)
			{
				msg::info("   Net %d is congested", inet);
			}
		}
	}
}

namespace router
{

	bool run_partitioned_parallel_routing_iteration(
		std::unordered_set<int>& congested_nets,
		Netlist& netlist,
		int num_threads
	)
	{
		namespace pg = partitioned::global;
		pg::rtq.reset(
			new partitioned::router_task_q_t(num_threads)
		);

		pg::rtq->attachCallback(partitioned::dispatcher);


		bool iteration_result = true;
		for(size_t inet = 0; inet < netlist.size(); ++inet)
		{
			auto find_it = congested_nets.find(inet);
			if (find_it != congested_nets.end())
			{
				congested_nets.erase(find_it);
				rip_up_net(netlist.at(inet));
			}
		}

		pg::rtq.reset();

		return iteration_result;
	}

	bool run_parallel_routing_iteration(std::unordered_set<int>& congested_nets, 
		Netlist& netlist,
		int num_threads)
	{
		bool iteration_result = true;
		// Serial initialization for each net
		for(size_t inet = 0; inet < netlist.size(); ++inet)
		{
			rip_up_net(netlist.at(inet));
		}

		rt_assert(num_threads > 1);

		msg::info("   Router claiming %d threads.", num_threads);

		// Initialize threads
		tbb::task_scheduler_init init_threads(num_threads);
		// Parallel routing
		tbb::parallel_for(size_t(0), netlist.size(), [&] (size_t inet) {
			bool net_result = true;	
			if(!netlist.at(inet).routed())
			{
				net_result = route_net(netlist.at(inet));
			}
			rt_assert(net_result);
		}
		);
		for(size_t inet = 0; inet < netlist.size(); ++inet)
		{
			bool net_result = true;
			if(netlist.at(inet).routed())
			{
				net_result = commit_route(netlist.at(inet));
			}

			if(!net_result)
			{
				congested_nets.insert(inet);
			}
			iteration_result = iteration_result && net_result;
		}
		return iteration_result;
	}

	bool run_serial_routing_iteration(std::unordered_set<int>& congested_nets, Netlist& netlist)
	{
		bool iteration_result = true;
		for(size_t inet = 0; inet < netlist.size(); ++inet)
		{
			bool net_result = true;
			bool should_rip_up = false;
			auto find_it = congested_nets.find(inet);
			if (find_it != congested_nets.end())
			{
				congested_nets.erase(find_it);
				should_rip_up = true;
			}
			else if (router_iteration_ >= utils::get_num_router_iterations()/10)
			{
				should_rip_up = true;
			}
			if (should_rip_up)
			{
				rip_up_net(netlist.at(inet));
			}
			if(!netlist.at(inet).routed())
			{
				net_result = route_net(netlist.at(inet));
				net_result = commit_route(netlist.at(inet)) && net_result;
			}
			if(!net_result)
			{
				congested_nets.insert(inet);
			}
			iteration_result = iteration_result && net_result;
		}
		return iteration_result;
	}

	bool run_route_algorithm(const Graph& g, Netlist& netlist)
	{
		graph_ = &g;
		netlist_ = &netlist;
		bool result = false;

		build_rr_nodes();
		build_pin_to_net_map();

		std::unordered_set<int> congested_nets;
		float congestion_change = 0.0;
		size_t num_congested_nodes = 0;

		int num_threads = utils::get_num_threads();
		if(num_threads == -1)
                {
			num_threads = tbb::task_scheduler_init::default_num_threads();
		}

		for(router_iteration_ = 0; 
			router_iteration_ < utils::get_num_router_iterations();
			++router_iteration_)
		{
			auto begin_wc = std::chrono::system_clock::now();
			auto begin_cpu = clock();
			msg::info("   Beginning routing iteration %d", router_iteration_);
			//report_nets(netlist);
			bool iteration_result = true;
			adjust_base_penalties();

			if(router_iteration_ == 0 && num_threads > 1)
			{
				iteration_result = run_parallel_routing_iteration(congested_nets, netlist, num_threads);
			}
			else
			{
				iteration_result = run_serial_routing_iteration(congested_nets, netlist);
			}

			expand_congested_nets(congested_nets);

			iteration_result = congested_nets.empty();

			auto end_wc = std::chrono::system_clock::now();
			auto end_cpu = clock();

			std::chrono::duration<double> elapsed_seconds = end_wc - begin_wc;
			double wc_time = elapsed_seconds.count();
			double cpu_time = (end_cpu - begin_cpu) / CLOCKS_PER_SEC;

			msg::info("   Iteration %d took %.2fs CPU, %.2fs WC",
				router_iteration_,
				cpu_time, wc_time);
			
			if(iteration_result)
			{
				result = true;
				break;
			}

			rt_assert(congested_nets.size() > 0);

			if(router_iteration_ == 0)
			{
				congestion_change = 100.0;
				num_congested_nodes = congested_nets.size();
			}
			else
			{

				congestion_change = static_cast<float>(
					static_cast<int>(congested_nets.size()) - static_cast<int>(num_congested_nodes)) * 100.0
					/ static_cast<float>(num_congested_nodes);
				num_congested_nodes = congested_nets.size();
			}



			if(router_iteration_ == utils::get_num_router_iterations())
			{
				report_congestion_stats(netlist, congested_nets);
			}
			else
			{
				msg::info("      %llu nets are congested. %.2f%% change", congested_nets.size(), 
					congestion_change);
				//std::sort(netlist.begin(), netlist.end(), compare_net);
				//build_pin_to_net_map();
			}
			if(router_iteration_ < NEGOTIATED_CONGESTION_TRANSITION &&
				!utils::disable_complete_ripup())
			{
				rip_up_all_nets(netlist);
			}

			if(router_iteration_ == 0)
			{
				CONGESTION_AVOIDANCE_PENALTY = 2;
			}
			if(router_iteration_ < 20)
			{
				CONGESTION_AVOIDANCE_PENALTY *= 1.5;
			}
			if(router_iteration_ == NEGOTIATED_CONGESTION_TRANSITION)
			{
				CONGESTION_BASE_PENALTY = 10;
			}
			else if(router_iteration_ < 60)
			{
				CONGESTION_AVOIDANCE_PENALTY *= 1.01;
			}
			else
			{
				CONGESTION_AVOIDANCE_PENALTY *= 1.001;
			}
			msg::info("      Congestion penalty is now %llu", CONGESTION_AVOIDANCE_PENALTY);
		}

		if(result)
		{
			check_routed_netlist(netlist);
		}
		cleanup();
		return result;
	}
}
