
OBJS = Node.o Graph.o main.o Net.o Flow.o Utils.o RouteAlgorithm.o graphics.o
TEST_OBJS = RouterUnitTest.o

CFLAGS =  -MP -MD -Wall -std=c++11 -DBOOST_TEST_DYN_LINK
LDFLAGS = -rdynamic -Wl,-rpath=./libs
LIBDIR = -Llibs
LIBS = -lX11 -lXft -lfontconfig -ltbb -ltbbmalloc_proxy
PARALLEL_LIBS = -lpthread -ltbb -ltbbmalloc_proxy
TEST_LIBS = -lboost_unit_test_framework
TEST_LIBS += $(PARALLEL_LIBS)
INCLUDEDIR = -I. -Iinclude
CXX = clang++
DEBUG = 0
CBEGIN = $(shell if [ -f "/etc/redhat-release" ]; then echo -n "scl enable devtoolset-2 '"; else echo -n ""; fi;)
CEND = $(shell if [ -f "/etc/redhat-release" ]; then echo -n "'"; else echo -n ""; fi;)

ifeq ($(DEBUG), 1)
        CFLAGS += -O0 -g -DDEBUG=1
else
        CFLAGS += -O2
endif

# Don't omit frame pointer when profiling so that profiling tools
# see a reasonable stack trace
ifeq ($(PROFILE), 1)
    CFLAGS += -fno-omit-frame-pointer -fno-inline
endif

#scl enable devtoolset-2 'bash'

CFLAGS += $(shell pkg-config --cflags freetype2) # evaluates to the correct include flags for the freetype headers

all: test

Router: $(OBJS)
	@echo Linking $@...
	@$(CBEGIN)$(CXX) $(CFLAGS) $(LDFLAGS) $(LIBDIR) $(OBJS) $(LIBS) -o Router$(CEND)

RouterUnitTest: $(TEST_OBJS)
	@echo Linking $@...
	@$(CBEGIN)$(CXX) $(CFLAGS) $(LDFLAGS) $(LIBDIR) $(TEST_OBJS) $(TEST_LIBS) -o $@$(CEND)


HeapTest: ParallelHeap.h ParallelHeapTest.cpp
	@echo Compiling and Linking $@...
	@$(CBEGIN)$(CXX) $(CFLAGS) $(INCLUDEDIR) $(LDFLAGS) $(LIBDIR) $(PARALLEL_LIBS) ParallelHeapTest.cpp -o $@$(CEND)
	@echo Running $@...
	@./$@

%.o: %.cpp
	@echo Compiling $<...
	@$(CBEGIN)$(CXX) $(CFLAGS) $(INCLUDEDIR) $< -c$(CEND)

clean:
	@-rm -rf *.o Router HeapTest RouterUnitTest *.d *routed_netlist.txt rr_graph.txt

unit_test: RouterUnitTest
	@echo ..................................
	@echo Running Unit Tests
	@./RouterUnitTest

reg_test: Router
	@echo ..................................
	@echo Running Router regression tests...
	@echo -n Testing circuit 1...
	@./Router cct1.txt -no_graphics 2>&1 > test.out && echo OK
	@echo -n "Testing circuit 1 (serial)..."
	@./Router cct1.txt -no_graphics -num_threads=1 2>&1 > test.out && echo OK
	@echo -n Testing circuit 2...
	@./Router cct2.txt -no_graphics 2>&1 >> test.out && echo OK
	@echo -n Testing circuit 3...
	@./Router cct3.txt -no_graphics 2>&1 >> test.out && echo OK
	@echo -n "Testing circuit 3 (serial)..."
	@./Router cct3.txt -no_graphics -num_threads=1 2>&1 >> test.out && echo OK
	@echo -n Testing circuit 4...
	@./Router cct4.txt -no_graphics 2>&1 >> test.out && echo OK
	@echo -n "Testing circuit 5 (determinism check)..."
	@-rm -f *routed_netlist.txt rr_graph.txt
	@./Router cct5.txt -no_graphics -debug_dump 2>&1 >> test.out && echo -n OK...
	@mv routed_netlist.txt first_routed_netlist.txt
	@./Router cct5.txt -no_graphics -debug_dump 2>&1 >> test.out && echo -n OK...
	@mv routed_netlist.txt second_routed_netlist.txt
	@diff first_routed_netlist.txt second_routed_netlist.txt 2>&1 > diff.out && echo MATCH
	@-rm -f *routed_netlist.txt rr_graph.txt
	@echo -n "Testing circuit 6 (determinism check)..."
	@-rm -f *routed_netlist.txt rr_graph.txt
	@./Router cct6.txt -no_graphics -debug_dump 2>&1 >> test.out && echo -n OK...
	@mv routed_netlist.txt first_routed_netlist.txt
	@./Router cct6.txt -no_graphics -debug_dump 2>&1 >> test.out && echo -n OK...
	@mv routed_netlist.txt second_routed_netlist.txt
	@diff first_routed_netlist.txt second_routed_netlist.txt 2>&1 >> diff.out && echo MATCH
	@-rm -f *routed_netlist.txt rr_graph.txt

test: unit_test reg_test
	
package: Router
	@echo Creating archive of repository
	@git archive --format tar master -o assignment1.tar
	@echo Compressing archive
	@gzip --best assignment1.tar

-include $(OBJS:.o=.d)

