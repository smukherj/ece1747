#ifndef NODE_H
#define NODE_H

namespace router
{
    class Node
    {
    public:
        enum Type
        {
            INVALID,
            TERMINAL,
            VTRACK,
            HTRACK,
            SWITCH_EAST,
            SWITCH_WEST,
            SWITCH_NORTH,
            SWITCH_SOUTH,
            NUM_TYPES
        };

        typedef unsigned long long ID;

        Node();
        ~Node();

        Type type() const { return m_type; }
        ID id() const { return m_id; }
        int x() const { return m_x; }
        int y() const { return m_y; }
        int z() const { return m_z; }
        bool valid() const;
        bool is_wire() const;
        bool is_switch() const;

        void set_type(Type t);
        void set_id(ID id);
        void set_x(int x);
        void set_y(int y);
        void set_z(int z);
        void finalize();

        static void check_type(Type t);
        static const char* type_str(Type t);
        static const ID ILLEGAL_ID;

    private:
        Type m_type;
        ID m_id;
        int m_x;
        int m_y;
        int m_z;
        bool m_finalized;

        // Assert not finalized
        void anf();
    };
}

#endif /* NODE_H */