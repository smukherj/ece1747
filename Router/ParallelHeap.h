#ifndef PARALLEL_HEAP_H
#define PARALLEL_HEAP_H

#include <algorithm>
#include <array>
#include <future>
#include <utility>
#include <vector>

#include "tbb/atomic.h"

#include "parallel_task_queue.h"

// Node, push if true else pop
template <class T>
using ThreadData = std::pair<T, bool>;

/// Create a templated parallel min-heap structure that can specify the number
/// of sub-heaps (N). To achieve parallelism, multiple push()/pop() calls can
/// run concurrently (non-blocking). top() and clear() are blocking.
template <class T, int N, class Compare = std::greater<T>>
class ParallelHeap {
public:
  ParallelHeap() : m_task_scheduler(N)
  {
    m_running_threads = 0;
    for (int i = 0; i < N; i++) {
      m_task_scheduler.attachCallback(i, [this, i] (const ThreadData<T>& data) {
        std::vector<T>& heap = m_heaps[i];
        bool push = data.second;
        if (push) {
          heap.push_back(data.first);
          std::push_heap(heap.begin(), heap.end(), Compare());
        } else {
          std::pop_heap(heap.begin(), heap.end(), Compare());
          heap.pop_back();
        }
        --m_running_threads;
      });
    }

    m_task_scheduler.dispatch();
  }

  bool empty() const { return m_size == 0U; }
  size_t size() const { return m_size; }
  const T& top() const
  {
    synchronize();

    m_top_index = get_top_index();
    return m_heaps[m_top_index].front();
  }

  // Non-blocking!
  void pop()
  {
    if (m_top_index != -1) {
      m_next_index = m_top_index;
    } else {
      synchronize();

      m_next_index = get_top_index();
    }
    m_top_index = -1;
    --m_size;

    ++m_running_threads;
    m_task_scheduler.addTask(m_next_index, std::make_pair(T(), false));
  }

  // Non-blocking!
  void push(const T& t)
  {
    m_top_index = -1;
    if (++m_next_index == N) m_next_index = 0;
    ++m_size;

    ++m_running_threads;
    m_task_scheduler.addTask(m_next_index, std::make_pair(t, true));
  }

  void clear()
  {
    synchronize();

    m_size = 0U;
    for (auto& heap : m_heaps) {
      heap.clear();
    }
  }

private:
  int get_top_index() const
  {
    auto min_it = std::min_element(m_heaps.begin(), m_heaps.end(), [] (const std::vector<T>& c1, const std::vector<T>& c2) {
      if (c1.empty()) return false;
      if (c2.empty()) return true;
      return Compare()(c2.front(), c1.front());
    });
    return static_cast<int>(std::distance(m_heaps.begin(), min_it));
  }

  void synchronize() const
  {
    while (m_running_threads != 0) ;
  }

  mutable int m_top_index = -1; // Cache to speed up top() then pop()
  int m_next_index = 0;
  size_t m_size = 0U;
  std::array<std::vector<T>, N> m_heaps;

  // Parallel data
  parallel::targeted_task_scheduler<ThreadData<T>> m_task_scheduler;
  tbb::atomic<size_t> m_running_threads;
};

#endif
