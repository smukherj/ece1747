#ifndef PARALLEL_TASK_QUEUE_H
#define PARALLEL_TASK_QUEUE_H

#include <tbb/task_scheduler_init.h>
#include <tbb/concurrent_queue.h>
#include <thread>
#include <vector>
#include <functional>
#include <boost/optional.hpp>
#include <cstdio>
#include <cassert>

namespace parallel
{

	// Creates a thread pool and the user specifies
	// the thread they want the task to run on

	// Another different between targeted_task_scheduler
	// and task_scheduler is that in targeted_task_scheduler
	// threads are not actually dispatched in the constructor.
	// There is a separate dispatch call that does this. This
	// is to allow the user to attach per thread custom callback
	// functions and set debug mode before the threads begin
	// running
	template <class DataType>
	class targeted_task_scheduler
	{
	private:
		static void empty_callback(DataType& d) {}

		void initialize_threads(int num_threads)
		{
			m_thread_callbacks.resize(num_threads, empty_callback);
			m_task_queues.resize(num_threads);
			m_threads.resize(num_threads);
		}

	public:
		using thread_callback_func_t = std::function<void(DataType&)>;

		targeted_task_scheduler()
		{
			initialize_threads(tbb::task_scheduler_init::default_num_threads());
		}

		targeted_task_scheduler(int num_threads)
		{
			initialize_threads(num_threads);
		}

		~targeted_task_scheduler()
		{
			// Only need to instruct the threads to quit
			// if they were actually dispatched!
			if(m_dispatched)
			{
				// Signal the threads to quit
				for(size_t i = 0; i < m_threads.size(); ++i)
				{
					m_task_queues[i].push(boost::none);
				}

				// Join all threads
				for(auto& t : m_threads)
				{
					t.join();
				}
			}
		}

		// Actually starts the threads
		void dispatch()
		{
			// Can't dispatch multiple times!
			assert(m_dispatched != true);

			auto thread_fn = [this] (int tid)
			{
				auto thread_callback_fn = m_thread_callbacks.at(tid);

				if(m_debug)
				{
					printf("Thread %d is alive\n", tid);
				}
				size_t num_tasks = 0;

				// Keep calling m_thread_callback on data objects
				// popped from the queue until we pop boost::none
				while(true)
				{
					boost::optional<DataType> d_opt;
#ifdef DEBUG
					m_task_queues.at(tid).pop(d_opt);
#else
					m_task_queues[tid].pop(d_opt);

					++num_tasks;
#endif
					if(d_opt == boost::none)
					{
						break;
					}

					auto& data = *d_opt;
					thread_callback_fn(data);
				}

				if(m_debug)
				{
					printf("Thread %d is done. %zu tasks processed\n", tid, num_tasks);
				}
			};

			for(size_t i = 0; i < m_threads.size(); ++i)
			{
				m_threads[i] = std::move(std::thread(thread_fn, i));
			}

			m_dispatched = true;
		}

		bool dispatched() const { return m_dispatched; }

		size_t num_threads() const { return m_threads.size(); }

		void attachCallback(thread_callback_func_t f)
		{
			assert(!m_dispatched);

			for(auto& callback : m_thread_callbacks)
			{
				callback = f;
			}
		}

		void attachCallback(size_t tid, thread_callback_func_t f)
		{
			assert(!m_dispatched);

#ifdef DEBUG
			m_thread_callbacks.at(tid) = f;
#else
			m_thread_callbacks[tid] = f;
#endif
		}


		void addTask(size_t tid, DataType&& d)
		{
#ifdef DEBUG
			assert(m_dispatched);
			m_task_queues.at(tid).push(d);
#else
			m_task_queues[tid].push(d);
#endif
		}

		void addTask(size_t tid, const DataType& d)
		{
#ifdef DEBUG
			assert(m_dispatched);
			m_task_queues.at(tid).push(d);
#else
			m_task_queues[tid].push(d);
#endif
		}

		void setDebug(bool debug)
		{
			assert(!m_dispatched);
			m_debug = debug;
		}

	private:
		using task_queue_t = tbb::concurrent_bounded_queue<boost::optional<DataType>>;

		std::vector<std::thread> m_threads;
		std::vector<thread_callback_func_t> m_thread_callbacks;
		std::vector<task_queue_t> m_task_queues;

		bool m_debug = false;
		bool m_dispatched = false;

	};

	// Creates a thread pool and tasks are randonly
	// allocated to an available thread
	template <class DataType>
	class task_scheduler
	{
	private:
		static void empty_callback(DataType& d) {}

		void initialize_threads(int num_threads)
		{
			auto thread_fn = [this] ()
			{
				// Keep calling m_thread_callback on data objects
				// popped from the queue until we pop boost::none
				while(true)
				{
					boost::optional<DataType> d_opt;
					m_task_queue.pop(d_opt);
					if(d_opt == boost::none)
					{
						break;
					}

					auto& data = *d_opt;
					m_thread_callback(data);
				}
			};

			for(int i = 0; i < num_threads; ++i)
			{
				m_threads.emplace_back(std::thread(thread_fn));
			}
		}

	public:
		using thread_callback_func_t = std::function<void(DataType&)>;

		task_scheduler() :
		m_thread_callback(empty_callback)
		{
			initialize_threads(tbb::task_scheduler_init::default_num_threads());
		}

		task_scheduler(int num_threads) :
		m_thread_callback(empty_callback)
		{
			initialize_threads(num_threads);
		}

		task_scheduler(int num_threads, thread_callback_func_t f) :
		m_thread_callback(f)
		{
			initialize_threads(num_threads);
		}

		task_scheduler(thread_callback_func_t f) :
		m_thread_callback(f)
		{
			initialize_threads(tbb::task_scheduler_init::default_num_threads());
		}

		~task_scheduler()
		{
			// Signal the threads to quit
			for(size_t i = 0; i < m_threads.size(); ++i)
			{
				m_task_queue.push(boost::none);
			}

			// Join all threads
			for(auto& t : m_threads)
			{
				t.join();
			}
		}

		void attachCallback(thread_callback_func_t f)
		{
			m_thread_callback = f;
		}


		void addTask(DataType&& d)
		{
			m_task_queue.push(d);
		}

		void addTask(const DataType& d)
		{
			m_task_queue.push(d);
		}

	private:
		std::vector<std::thread> m_threads;

		std::function<void(DataType&)> m_thread_callback;
		tbb::concurrent_bounded_queue<boost::optional<DataType>> m_task_queue;
	};
}
#endif
